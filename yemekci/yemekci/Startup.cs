﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Yemekci.Startup))]
namespace Yemekci
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
