﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Yemekci.Models;

namespace Yemekci.DAL
{
    public class YemekciBaslangicData : System.Data.Entity.DropCreateDatabaseIfModelChanges<YemekciContext>
    {
        protected override void Seed(YemekciContext context)
        {
            #region Ürünler

            var Urunler = new List<Urun>
            {
                new Urun {Kategori="Kahvaltılar",Ad="Kahvaltı Tabağı",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Kahvaltılar",Ad="Açık Büfe Kahvaltı",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Ekstra Kahvaltılıklar",Ad="Menemen",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ekstra Kahvaltılıklar",Ad="Omlet",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ekstra Kahvaltılıklar",Ad="Tereyağlı Yumurta",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ekstra Kahvaltılıklar",Ad="Paçanga",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ekstra Kahvaltılıklar",Ad="Söğüş",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ekstra Kahvaltılıklar",Ad="Peynir Tabağı",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ekstra Kahvaltılıklar",Ad="Meyve Tabağı",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Çorbalar",Ad="Mercimek Çorbası",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Çorbalar",Ad="Ezogelin Çorbası",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Çorbalar",Ad="Ekşili Tavuk Çorbası",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Ara Sıcaklar",Ad="Elma Dilim Patates",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ara Sıcaklar",Ad="Parmak Patates",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ara Sıcaklar",Ad="Paçanga Böreği",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ara Sıcaklar",Ad="Su Böreği",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ara Sıcaklar",Ad="Sigara Böreği",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ara Sıcaklar",Ad="Mantar Graten",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Salatalar",Ad="Mevsim Salata",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Salatalar",Ad="Çoban Salata",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Salatalar",Ad="Kaşık Salata",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Salatalar",Ad="Tonbalıklı Salata",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Salatalar",Ad="Tavuk Salata",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Ana Yemekler",Ad="Güveçte Kuru Fasulye",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Çoban Kavurma",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Piliç Kavurma",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Kiremitte Kuşbaşı",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Kiremitte Köfte",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Etli Yufka Kebabı",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Tavuklu Yufka Kebabı",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Tavuk Şinitzel",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Domates Soslu Bonfile",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Fırında Kuzu Tandır",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Ana Yemekler",Ad="Tavuklu Yufka Kebabı",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Balık Çeşitleri",Ad="Ala Balık Izgara",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Balık Çeşitleri",Ad="Levrek Izgara",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Balık Çeşitleri",Ad="Çipura Izgara",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Balık Çeşitleri",Ad="Somon Izgara",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Balık Çeşitleri",Ad="Hamsi Tava",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Balık Çeşitleri",Ad="Mezgit Tava",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Tatlılar",Ad="Aşure",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Tatlılar",Ad="Güllaç",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Tatlılar",Ad="Cevizli Kalburabastı",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Tatlılar",Ad="Kadayıf",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Tatlılar",Ad="Künefe",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Tatlılar",Ad="Şekerpare",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Tatlılar",Ad="Ayva Tatlısı",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Sıcak İçecekler",Ad="Nescafe",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Sıcak İçecekler",Ad="Türk Kahvesi",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Sıcak İçecekler",Ad="Fincan Çay",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Sıcak İçecekler",Ad="Çay",Detay="",Adet=10,Fiyat=10,ResimYol=""},

                new Urun {Kategori="Soğuk İçecekler",Ad="Taze Sıkılmış Portakal Suyu",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Soğuk İçecekler",Ad="Meyve Suları",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Soğuk İçecekler",Ad="Gazlı İçecekler",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Soğuk İçecekler",Ad="Maden Suyu",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Soğuk İçecekler",Ad="Şalgam",Detay="",Adet=10,Fiyat=10,ResimYol=""},
                new Urun {Kategori="Soğuk İçecekler",Ad="Ayran",Detay="",Adet=10,Fiyat=10,ResimYol=""}

            };

            Urunler.ForEach(s => context.Uruns.Add(s));
            context.SaveChanges();

            #endregion


            #region Uyeler

            var Uyeler = new List<Uye>
            {
                new Uye {Ad="Gökhan",Soyad="Karagöz",Eposta="g140910034@sakarya.edu.tr",Sifre="12345",Rutbe="Yönetici" },
                new Uye {Ad="Ahmet",Soyad="Sönmez",Eposta="ahmet@sakarya.edu.tr",Sifre="12345",Rutbe="Üye" },
                new Uye {Ad="Ayşe",Soyad="Dinker",Eposta="ayse@sakarya.edu.tr",Sifre="12345",Rutbe="Üye" }
            };

            Uyeler.ForEach(s => context.Uyes.Add(s));
            context.SaveChanges();

            #endregion
        }
    }
}