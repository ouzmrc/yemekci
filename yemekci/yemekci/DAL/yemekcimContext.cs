﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Yemekci.Models;

namespace Yemekci.DAL
{
    public class YemekciContext : DbContext
    {
        public YemekciContext() : base("YemekciContext") { }
        public DbSet<Uye> Uyes { get; set; }
        public DbSet<Urun> Uruns { get; set; }
        public DbSet<Iletisim> Iletisims { get; set; }
    }
}